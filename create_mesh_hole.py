from dolfin import * 
from mshr import *

center = dolfin.Point(0.01, 0.01)
radius = 0.005

# Define domains
domain = Rectangle(Point(0.0, 0.0), Point(0.02,0.02)) - Circle(center, radius) \

# Generate mesh
mesh = generate_mesh(domain, 20)
plot(mesh)

# Save mesh to file
file = File("mesh_hole.xml")
file << mesh

interactive()

