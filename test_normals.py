
from dolfin import *

class Left(SubDomain): 
  def inside(self, x, on_boundary): 
    if x[0] < 0.5: return True 
    return False	

class Interface(SubDomain): 
  def inside(self, x, on_boundary): 
    if near(x[0], 0.5): return True 
    return False	



mesh = UnitSquareMesh(4,4, "crossed")
V = VectorFunctionSpace(mesh, "CG", 1)

subdomains = CellFunction("size_t", mesh) 
left = Left()
subdomains.set_all(0)
left.mark(subdomains, 1)

interior_facets = FacetFunction("size_t", mesh)
interface = Interface()
interior_facets.set_all(0)
interface.mark(interior_facets, 2)

plot(subdomains)
plot(interior_facets)




f = Constant((1,1))

u = TrialFunction(V)
v = TestFunction(V)
n = FacetNormal(mesh)

#dx = Measure("dx", domain=mesh, subdomain_data=subdomains)
dS = Measure("dS", domain=mesh, subdomain_data=interior_facets)
a = inner(u,v)*dx 
L = inner(n('+'),avg(v))*dS(2)

b = assemble(L)
print b.array() 

U = Function(V) 

solve(a==L, U)

print U.vector().array()

plot(mesh)
plot(U)
interactive()















