from dolfin import * 
from mshr import *

center = dolfin.Point(0.01, 0.01)
radius = 0.005

# Define domains
domain = Rectangle(Point(0.0, 0.0), Point(0.02,0.02))
domain.set_subdomain(1, Circle(Point(center[0], center[1]), radius))
# Generate mesh
mesh = generate_mesh(domain, 20)
plot(mesh)

# Save mesh to file
file = File("mesh.xml")
file << mesh

interactive()

